package andrej.biro.nipme.base;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import eu.inloop.viewmodel.AbstractViewModel;
import eu.inloop.viewmodel.IView;
import eu.inloop.viewmodel.base.ViewModelBaseFragment;

/**
 * Created by andrej on 19.11.2016.
 */

public abstract class BaseFragment<T extends IView, R extends AbstractViewModel<T>, B extends ViewDataBinding, HANDLER extends BaseHandler> extends ViewModelBaseFragment<T, R> implements IView {
    protected HANDLER handler;
    protected B view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = DataBindingUtil.inflate(inflater, getLayoutId(), container, false);
        alterViews();

        return view.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setModelView(getViewClass());
        setModelViewData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        handler = (HANDLER) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        handler = null;
    }

    protected abstract @LayoutRes int getLayoutId();

    @NonNull
    protected abstract T getViewClass();

    protected abstract void setModelViewData();

    protected abstract void alterViews();

}
