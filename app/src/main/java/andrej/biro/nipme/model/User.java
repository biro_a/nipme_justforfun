package andrej.biro.nipme.model;

import android.support.annotation.StringRes;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import andrej.biro.nipme.R;
import andrej.biro.nipme.networking.RetrofitFactory;

/**
 * Created by andrej on 26.12.2016.
 */

public class User implements Serializable {
    @SerializedName("ContactInformation")
    private String contactInformation;
    @SerializedName("Distance")
    private int distance;
    @SerializedName("FriendStatus")
    private FriendStatus friendStatus;
    @SerializedName("Guid")
    private String guid;
    @SerializedName("Latitude")
    private double latitude;
    @SerializedName("Longitude")
    private double longitude;
    @SerializedName("MessageText")
    private String messageText;
    @SerializedName("Motto")
    private String motto;
    @SerializedName("Login")
    private String login;
    @SerializedName("Name")
    private String name;
    @SerializedName("PublicNip")
    private String publicNip;
    @SerializedName("Status")
    private int status;
    @SerializedName("Unreaded")
    private int unreaded;
    @SerializedName("UserId")
    private int userId;

    public String getContactInformation() {
        return contactInformation;
    }

    public @StringRes Integer getDistanceForUserList() {
        if (distance < 0) return null;

        if (distance < 500) {
            return R.string.very_close;
        } else if (distance < 1000) {
            return R.string.less_than_kilometer;
        }

        return R.string.more_than_kilometer;
    }

    public FriendStatus getFriendStatus() {
        return friendStatus;
    }

    public String getMessageText() {
        return messageText;
    }

    public String getMotto() {
        return motto;
    }

    public String getLogin() {
        return login;
    }

    public String getName() {
        return name;
    }

    public String getPublicNip() {
        return publicNip;
    }

    public int getUnreaded() {
        return unreaded;
    }

    public int getUserId() {
        return userId;
    }

    public String getProfilePhotoUrl() {
        StringBuilder builder = new StringBuilder();
        builder.append(RetrofitFactory.BASE_URL )
                .append("img/profile/" )
                .append(guid)
                .append(".png");
        return builder.toString();
    }

    public enum FriendStatus implements Serializable {
        @SerializedName("0")
        NONE,
        @SerializedName("1")
        FRIEND,
        @SerializedName("2")
        BLOCKED
    }
}
