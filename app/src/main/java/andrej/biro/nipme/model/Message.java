package andrej.biro.nipme.model;

import com.google.gson.annotations.SerializedName;

import andrej.biro.nipme.Session;

/**
 * Created by andrej on 7.1.2017.
 */

public class Message {
    @SerializedName("Id")
    private long id;
    @SerializedName("DateSent")
    private String dateSent;
    @SerializedName("Received")
    private String received;
    @SerializedName("ReceiverId")
    private long receiverId;
    @SerializedName("SenderId")
    private long senderId;
    @SerializedName("Text")
    private String text;
    @SerializedName("Uuid")
    private String uuid;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDateSent() {
        return dateSent;
    }

    public void setDateSent(String dateSent) {
        this.dateSent = dateSent;
    }

    public String getReceived() {
        return received;
    }

    public void setReceived(String received) {
        this.received = received;
    }

    public long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(long receiverId) {
        this.receiverId = receiverId;
    }

    public long getSenderId() {
        return senderId;
    }

    public void setSenderId(long senderId) {
        this.senderId = senderId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isMessageMy() {
       return senderId == Session.getInstance().getLoggedLoggedUser().getUserId();
    }
}
