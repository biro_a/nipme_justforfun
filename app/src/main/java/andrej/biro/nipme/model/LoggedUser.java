package andrej.biro.nipme.model;

import com.google.gson.annotations.SerializedName;

import andrej.biro.nipme.networking.RetrofitFactory;

/**
 * Created by andrej on 19.11.2016.
 */

public class LoggedUser {
    @SerializedName("Login")
    private String login;
    @SerializedName("ContactInformation")
    private String contactInformation;
    @SerializedName("motto")
    private String motto;
    @SerializedName("Guid")
    private String guid;
    @SerializedName("Id")
    private long userId;
    @SerializedName("Name")
    private String name;
    @SerializedName("SID")
    private SID sid;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getToken() {
        return this.sid.token;
    }

    public void setSid(SID sid) {
        this.sid = sid;
    }

    public String getProfilePhotoUrl() {
        StringBuilder builder = new StringBuilder();
        builder.append(RetrofitFactory.BASE_URL )
                .append("img/profile/" )
                .append(guid)
                .append(".png");
        return builder.toString();
    }

    private static class SID {
        @SerializedName("Expiration")
        private String expiration;
        @SerializedName("Token")
        private String token;
    }

    @Override
    public String toString() {
        return "User{" +
                "login='" + login + '\'' +
                ", motto='" + motto + '\'' +
                ", guid='" + guid + '\'' +
                ", userId=" + userId +
                '}';
    }
}
