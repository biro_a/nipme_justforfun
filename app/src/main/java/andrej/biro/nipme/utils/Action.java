package andrej.biro.nipme.utils;

/**
 * Created by andrej on 13.11.2016.
 */

public interface Action {
    void call();
}
