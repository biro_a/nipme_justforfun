package andrej.biro.nipme.utils;

import android.view.View;

/**
 * Created by andrej on 7.1.2017.
 */

public interface OnItemClickListener {
    void onItemClick(View view, int position);
}
