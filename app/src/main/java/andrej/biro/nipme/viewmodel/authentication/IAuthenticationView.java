package andrej.biro.nipme.viewmodel.authentication;

import android.support.annotation.NonNull;

import andrej.biro.nipme.networking.NetworkError;
import andrej.biro.nipme.ui.authentication.AuthenticationFragment.LoginValidationInputErrorType;
import eu.inloop.viewmodel.IView;

/**
 * Created by andrej on 12.11.2016.
 */

public interface IAuthenticationView extends IView {
    void showLoginView();
    void showRegisterView();
    void startSpinnerAnimation();
    void successAuthenticate();
    void authenticationValidationInputFailed(@NonNull LoginValidationInputErrorType errorType);
    void onAuthenticationError(@NonNull NetworkError error);
}
