package andrej.biro.nipme.viewmodel.users;

import java.util.List;

import andrej.biro.nipme.model.User;
import eu.inloop.viewmodel.IView;

/**
 * Created by andrej on 17.12.2016.
 */

public interface IUserListView extends IView {
    void creteAdapter(List<User> users);
    void updateAdapter();
}
