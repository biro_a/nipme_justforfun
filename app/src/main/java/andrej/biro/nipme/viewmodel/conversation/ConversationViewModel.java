package andrej.biro.nipme.viewmodel.conversation;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import andrej.biro.nipme.Session;
import andrej.biro.nipme.model.Message;
import andrej.biro.nipme.model.User;
import andrej.biro.nipme.networking.NetworkError;
import andrej.biro.nipme.networking.conversation.ConversationRequest;
import andrej.biro.nipme.networking.conversation.ConversationRequestManager;
import andrej.biro.nipme.viewmodel.NetworkViewModel;

/**
 * Created by andrej on 7.1.2017.
 */

public class ConversationViewModel extends NetworkViewModel<IConversationView, List<Message>> {
    private static final int ITEM_COUNT = 10;

    private ConversationRequestManager requestManager;
    private User user;
    private List<Message> messages;
    private MaybeNetworkObserver subscriber;

    @Override
    public boolean isRequestingInformation() {
        return false;
    }

    @Override
    protected void onAuthenticationError(@NonNull NetworkError networkError) {

    }

    @Override
    protected void onNetworkError(Throwable throwable) {

    }

    @Override
    protected void onNetworkSuccess(List<Message> messages) {
        this.messages.addAll(messages);
        callViewAction(() -> getView().updateAdapter());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dispose(subscriber);
    }

    public void initiate(User user) {

        if (user == null) {
            throw new IllegalStateException("User has to be defined");
        }

        this.user = user;
        this.requestManager = ConversationRequestManager.getInstance();
        this.messages = new ArrayList<>();
        callViewAction(() -> getView().createAdapter(messages));
        sentGetMessagesRequest();

    }

    public void loadMessages() {
        sentGetMessagesRequest();
    }

    private void sentGetMessagesRequest() {
        subscriber = new MaybeNetworkObserver();
        ConversationRequest request = new ConversationRequest(user.getUserId(), messages.size(), ITEM_COUNT);
        requestManager.getConversation(Session.getInstance().getToken(), request)
                .subscribe(subscriber);
    }
}
