package andrej.biro.nipme.viewmodel.conversation;

import java.util.List;

import andrej.biro.nipme.model.Message;
import eu.inloop.viewmodel.IView;

/**
 * Created by andrej on 7.1.2017.
 */

public interface IConversationView extends IView {
    void createAdapter(List<Message> messages);
    void updateAdapter();
}
