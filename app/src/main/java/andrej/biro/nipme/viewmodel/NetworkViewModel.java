package andrej.biro.nipme.viewmodel;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.HttpException;

import java.io.IOException;

import andrej.biro.nipme.utils.Action;
import andrej.biro.nipme.networking.NetworkError;
import andrej.biro.nipme.utils.Constants;
import eu.inloop.viewmodel.AbstractViewModel;
import eu.inloop.viewmodel.IView;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.observers.DisposableMaybeObserver;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;

import static andrej.biro.nipme.utils.Constants.REQUEST_FAILED;
import static andrej.biro.nipme.utils.Constants.REQUEST_NONE;
import static andrej.biro.nipme.utils.Constants.REQUEST_RUNNING;
import static andrej.biro.nipme.utils.Constants.REQUEST_SUCCEEDED;


/**
 * Created by andrej on 19.11.2016.
 */

public abstract class NetworkViewModel<T extends IView, DATA> extends AbstractViewModel<T>{
    protected final String TAG = this.getClass().getName();
    protected @Constants.RequestState
    int requestState;
    protected Throwable lastError;

    public abstract boolean isRequestingInformation();

    public NetworkViewModel() {

        this.requestState = REQUEST_NONE;
    }

    public @Constants.RequestState
    int getRequestState() {

        if (isRequestingInformation()) {
            return REQUEST_RUNNING;
        }

        return requestState;
    }

    public Throwable getLastError() {

        return lastError;
    }

    protected void callViewAction(Action action) {
        if (getView() != null) {
            action.call();
        }
    }

    protected void dispose(Disposable subscriber) {
        if (subscriber != null && subscriber.isDisposed()) {
            subscriber.dispose();
        }
    }

    protected abstract void onAuthenticationError(@NonNull NetworkError networkError);

    protected abstract void onNetworkError(Throwable throwable);

    protected abstract void onNetworkSuccess(DATA data);

    private void handleNetworkError(Throwable e) {
        lastError = e;
        requestState = REQUEST_FAILED;

        NetworkError networkError = null;
        CompositeException exceptions = (CompositeException) e;
        for (Throwable throwable : exceptions.getExceptions()) {
            if (throwable instanceof HttpException) {
                HttpException httpException = (HttpException) throwable;
                try {
                    String errorString = httpException.response().errorBody().string();
                    Gson gson = new Gson();
                    networkError = gson.fromJson(errorString, NetworkError.class);
                    Log.d("NetworkError", networkError.toString());
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

            }
        }

        if (networkError == null) {
            onNetworkError(e);
        } else {
            onAuthenticationError(networkError);
        }
    }

    public class MaybeNetworkObserver extends DisposableMaybeObserver<DATA> {
        @Override
        public void onSuccess(DATA value) {
            requestState = REQUEST_SUCCEEDED;
            onNetworkSuccess(value);
        }

        @Override
        public void onError(Throwable e) {
            e.printStackTrace();
            handleNetworkError(e);
        }

        @Override
        public void onComplete() {

        }
    }

    public class SingleNetworkObserver extends DisposableSingleObserver<DATA> {

        @Override
        public void onSuccess(DATA value) {
            requestState = REQUEST_SUCCEEDED;
            onNetworkSuccess(value);
        }

        @Override
        public void onError(Throwable e) {
            onNetworkError(e);
        }
    }

    public class NetworkObserver extends DisposableObserver<DATA> {

        @Override
        public void onNext(DATA value) {
            onNetworkSuccess(value);
        }

        @Override
        public void onError(Throwable e) {
            handleNetworkError(e);
        }

        @Override
        public void onComplete() {}
    }
}
