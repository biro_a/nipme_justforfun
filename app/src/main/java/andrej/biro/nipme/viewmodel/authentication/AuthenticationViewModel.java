package andrej.biro.nipme.viewmodel.authentication;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import andrej.biro.nipme.R;
import andrej.biro.nipme.Session;
import andrej.biro.nipme.model.LoggedUser;
import andrej.biro.nipme.networking.NetworkError;
import andrej.biro.nipme.networking.authentication.AuthenticationRequestManager;
import andrej.biro.nipme.networking.authentication.login.LoginRequest;
import andrej.biro.nipme.networking.authentication.register.RegisterRequest;
import andrej.biro.nipme.ui.authentication.AuthenticationFragment.LoginValidationInputErrorType;
import andrej.biro.nipme.viewmodel.NetworkViewModel;

import static andrej.biro.nipme.ui.authentication.AuthenticationFragment.LoginValidationInputErrorType.LOGIN_PASSWORD_TOO_SHORT;
import static andrej.biro.nipme.ui.authentication.AuthenticationFragment.LoginValidationInputErrorType.LOGIN_USERNAME_TOO_SHORT;
import static andrej.biro.nipme.ui.authentication.AuthenticationFragment.LoginValidationInputErrorType.REGISTER_PASSWORD_TOO_SHORT;
import static andrej.biro.nipme.ui.authentication.AuthenticationFragment.LoginValidationInputErrorType.REGISTER_USERNAME_TOO_SHORT;

/**
 * Created by andrej on 12.11.2016.
 */

public class AuthenticationViewModel extends NetworkViewModel<IAuthenticationView, LoggedUser> {
    private AuthenticationRequestManager authenticationRequestManager;
    private LoginRequest loginRequest;
    private RegisterRequest registerRequest;

    /********************
    *                   *
    *   implementation  *
    *                   *
    *********************/
    @Override
    public boolean isRequestingInformation() {
        return authenticationRequestManager.isRequestingInformation();
    }

    @Override
    protected void onAuthenticationError(@NonNull NetworkError networkError) {
       callViewAction(() -> getView().onAuthenticationError(networkError));
    }

    @Override
    protected void onNetworkError(Throwable throwable) {
        NetworkError generalError = new NetworkError();
        generalError.setErrorType(NetworkError.ErrorType.GeneralError);
        callViewAction(() -> getView().onAuthenticationError(generalError));
    }

    @Override
    protected void onNetworkSuccess(LoggedUser loggedUser) {
        Session.getInstance().setLoggedLoggedUser(loggedUser);
        successAuthenticate();
    }

    public void initialize() {
        authenticationRequestManager = AuthenticationRequestManager.getInstance();
        loginRequest = new LoginRequest("user1", "heslo");
        registerRequest = new RegisterRequest();
    }

    /********************
    *                   *
    *    view's method  *
    *                   *
    *********************/
    public void showSignInView() {
        callViewAction(() -> getView().showLoginView());
    }

    public void showSignUpView() {
        callViewAction(() -> getView().showRegisterView());
    }

    public void onClickLogin() { login(); }

    public void onClickRegister() { register(); }

    public @StringRes int getErrorMessage(NetworkError error) {
       return  error == null ? R.string.empty_string : error.getErrorType().getErrorText();
    }
    /********************
    *                   *
    *      accessors    *
    *                   *
    *********************/
    public LoginRequest getLoginRequest() { return this.loginRequest; }
    public RegisterRequest getRegisterRequest() { return this.registerRequest; }

    /********************
    *                   *
    *  private methods  *
    *                   *
    *********************/
    private void login() {
        if (authenticationRequestManager.isRequestingInformation()) {
            return;
        }

        callViewAction(() -> getView().startSpinnerAnimation());
        LoginValidationInputErrorType errorType = getLoginValidationInputError(loginRequest.getLogin(),
                loginRequest.getPassword());

        if (errorType == null) {
            authenticationRequestManager
                    .login(loginRequest)
                    .subscribe(new MaybeNetworkObserver());
        } else {
            callViewAction(() -> getView().authenticationValidationInputFailed(errorType));
        }
    }

    private void register() {
        if (authenticationRequestManager.isRequestingInformation()) {
            return;
        }

        callViewAction(() -> getView().startSpinnerAnimation());
        LoginValidationInputErrorType errorType = getRegisterValidationInputError(registerRequest.getLogin(),
                registerRequest.getPassword());

        if (errorType == null) {
            authenticationRequestManager
                    .register(registerRequest)
                    .subscribe(new MaybeNetworkObserver());
        } else {
            callViewAction(() -> getView().authenticationValidationInputFailed(errorType));
        }
    }

    @Nullable
    private LoginValidationInputErrorType getLoginValidationInputError(String username, String password) {
        if (isUsernameInvalid(username)) {
            return LOGIN_USERNAME_TOO_SHORT;
        } else if (isPasswordInvalid(password)) {
            return LOGIN_PASSWORD_TOO_SHORT;
        }

        return null;
    }

    @Nullable
    private LoginValidationInputErrorType getRegisterValidationInputError(String username, String password) {
        if (isUsernameInvalid(username)) {
            return REGISTER_USERNAME_TOO_SHORT;
        } else if (isPasswordInvalid(password)) {
            return REGISTER_PASSWORD_TOO_SHORT;
        }

        return null;
    }

    private boolean isUsernameInvalid(String login) {
        return login == null || login.length() < 5;
    }

    private boolean isPasswordInvalid(String password) {
        return password == null || password.length() < 5;
    }

    private void successAuthenticate() {
        callViewAction(() -> getView().successAuthenticate());
    }

}
