package andrej.biro.nipme.viewmodel.users;

import android.support.annotation.NonNull;

import java.util.LinkedList;
import java.util.List;

import andrej.biro.nipme.Session;
import andrej.biro.nipme.model.User;
import andrej.biro.nipme.networking.NetworkError;
import andrej.biro.nipme.networking.user_service.UserServiceRequestManager;
import andrej.biro.nipme.networking.user_service.find.FindUsersRequest;
import andrej.biro.nipme.viewmodel.NetworkViewModel;

/**
 * Created by andrej on 17.12.2016.
 */

public class UserListViewModel extends NetworkViewModel<IUserListView, List<User>> {
    private FindUsersRequest findUsersRequest;
    private UserServiceRequestManager userServiceRequestManager;
    private SingleNetworkObserver subscriber;
    private List<User> userList;

    @Override
    public boolean isRequestingInformation() {
        return false;
    }

    @Override
    protected void onAuthenticationError(@NonNull NetworkError networkError) {
    }

    @Override
    protected void onNetworkError(Throwable throwable) {

    }

    @Override
    protected void onNetworkSuccess(List<User> users) {
        userList.clear();
        userList.addAll(users);
        callViewAction(() -> getView().updateAdapter());
    }

    public void initiate(int position) {
        this.userServiceRequestManager = UserServiceRequestManager.getInstance();
        this.findUsersRequest = new FindUsersRequest(position, "");
        this.userList = new LinkedList<>();
        callViewAction(() -> getView().creteAdapter(userList));
    }

    public void onResume() { sentFindUsersRequest(); }

    public void onPause() {
        dispose(subscriber);
    }

    public void findUsers() {
        sentFindUsersRequest();
    }

    public User getUser(int position) {
        if (userList != null) {
           return userList.get(position);
        }

        return null;
    }

    private void sentFindUsersRequest() {
        dispose(subscriber);
        subscriber = new SingleNetworkObserver();
        userServiceRequestManager.findUsers(Session.getInstance().getToken(), findUsersRequest)
                .subscribe(subscriber);
    }

}
