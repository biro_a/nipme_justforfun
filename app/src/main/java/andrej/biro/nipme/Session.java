package andrej.biro.nipme;

import andrej.biro.nipme.model.LoggedUser;

/**
 * Created by andrej on 18.12.2016.
 */

public final class Session {
    private static Session instance;
    private LoggedUser loggedLoggedUser;

    public static Session getInstance() {
        synchronized (Session.class) {
            if (instance == null) {
                instance = new Session();
            }
        }

        return instance;
    }

    private Session() {
    }


    public LoggedUser getLoggedLoggedUser() {
        return this.loggedLoggedUser;
    }

    public void setLoggedLoggedUser(LoggedUser loggedUser) {
       this.loggedLoggedUser = loggedUser;
    }

    public String getToken() {
        return this.loggedLoggedUser.getToken();
    }

}
