package andrej.biro.nipme.networking.authentication.register;

import andrej.biro.nipme.model.LoggedUser;
import andrej.biro.nipme.networking.authentication.register.exception.RegisterTechFailureException;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by andrej on 26.11.2016.
 */

public class RegisterAPIService {
    private IRegisterApi registerApi;
    private boolean isRequestingRegister;

    public RegisterAPIService(Retrofit retrofit) {
        this.registerApi = retrofit.create(IRegisterApi.class);
    }

    public Maybe<LoggedUser> register(RegisterRequest request) {

        return registerApi.register(request)
                .doOnSubscribe(disposable -> isRequestingRegister = true)
                .doOnTerminate(() -> isRequestingRegister = false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::handleLoginError)
                .singleElement();
    }

    public boolean isRequestingRegister() {
        return isRequestingRegister;
    }

    private Observable<LoggedUser> handleLoginError(Throwable throwable) {

        throw new RegisterTechFailureException();
    }
}
