package andrej.biro.nipme.networking.user_service.find;

import java.util.List;

import andrej.biro.nipme.model.User;
import andrej.biro.nipme.networking.authentication.login.exception.LoginTechFailureException;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by andrej on 25.12.2016.
 */

public class FindUsersAPIService {
    private IFindUsersApi findUsersApi;
    private boolean isRequesting;

    public FindUsersAPIService(Retrofit retrofit) {
        findUsersApi = retrofit.create(IFindUsersApi.class);
    }

    /**
     * @param token user token for identification
     * @param request request containing, which type of users we are trying to find
     * @return list of users
     */
    public Single<List<User>> findUsers(String token, FindUsersRequest request) {

        return   findUsersApi.findUsers(token, request)
                .doOnSubscribe(disposable -> isRequesting = true)
                .flatMap(Observable::fromIterable)
                .filter((user) -> !user.getLogin().isEmpty())
                .toList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(this::handleLoginError);
    }

    public boolean isRequesting() {
        return isRequesting;
    }

    private Observable<List<User>> handleLoginError(Throwable throwable) {
        throwable.printStackTrace();
        throw new LoginTechFailureException();
    }
}
