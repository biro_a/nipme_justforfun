package andrej.biro.nipme.networking.user_service.find;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andrej on 25.12.2016.
 */

public class FindUsersRequest {
    @SerializedName("Switch")
    private int type;
    @SerializedName("Name")
    private String name;

    public FindUsersRequest(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
