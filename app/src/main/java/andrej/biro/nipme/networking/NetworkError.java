package andrej.biro.nipme.networking;

import android.support.annotation.StringRes;

import com.google.gson.annotations.SerializedName;

import andrej.biro.nipme.R;

/**
 * Created by andrej on 19.11.2016.
 */

public class NetworkError {
    @SerializedName("ErrorType")
    private ErrorType errorType;

    @SerializedName("EventDetails")
    private String eventDetails;

    @SerializedName("ExceptionMessage")
    private String exceptionMessage;

    public ErrorType getErrorType() {
        return errorType;
    }

    public void setErrorType(ErrorType errorType) {
        this.errorType = errorType;
    }

    public String getEventDetails() {
        return eventDetails;
    }

    public void setEventDetails(String eventDetails) {
        this.eventDetails = eventDetails;
    }

    public String getExceptionMessage() {
        return exceptionMessage;
    }

    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }

    @Override
    public String toString() {
        return "NetworkError{" +
                "errorType=" + errorType +
                ", eventDetails='" + eventDetails + '\'' +
                ", exceptionMessage='" + exceptionMessage + '\'' +
                '}';
    }

    public enum ErrorType {
        @SerializedName("0")
        GeneralError(R.string.general_error),
        @SerializedName("1")
        Unauthorized(R.string.unauthorized),
        @SerializedName("2")
        DatabseReading(R.string.database_is_reading),
        @SerializedName("3")
        UserAlreadyExists(R.string.user_already_exists),
        @SerializedName("4")
        MailAlreadyExists(R.string.mail_already_exists),
        @SerializedName("5")
        DeviceAlreadyUsed(R.string.device_already_used),
        @SerializedName("6")
        WrongCredentials(R.string.wrong_credentials),
        @SerializedName("7")
        FriendsAlready(R.string.already_friends),
        @SerializedName("8")
        UserDoesNotExist(R.string.user_doesnt_exit),
        @SerializedName("9")
        NotFriends(R.string.not_friends),
        @SerializedName("10")
        UserNotActivated(R.string.user_not_activated),
        @SerializedName("11")
        ImageWriting(R.string.image_is_writing),
        @SerializedName("12")
        NoStream(R.string.no_stream);

        private final @StringRes int errorText;

        ErrorType(int errorText) {
            this.errorText = errorText;
        }

        public @StringRes int getErrorText() {
            return errorText;
        }
    }

}
