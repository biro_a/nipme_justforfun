package andrej.biro.nipme.networking.user_service.find;

import java.util.List;

import andrej.biro.nipme.model.User;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by andrej on 25.12.2016.
 */

public interface IFindUsersApi {
    @POST("ws/UserService.svc/findFriends")
    Observable<List<User>> findUsers(@Query("token") String token, @Body FindUsersRequest request);
}
