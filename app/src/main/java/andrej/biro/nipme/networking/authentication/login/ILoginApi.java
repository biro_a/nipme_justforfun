package andrej.biro.nipme.networking.authentication.login;

import andrej.biro.nipme.model.LoggedUser;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by andrej on 18.11.2016.
 */

interface ILoginApi {
    @POST("ws/AuthService.svc/login")
    Observable<LoggedUser> login(@Body LoginRequest request);
}
