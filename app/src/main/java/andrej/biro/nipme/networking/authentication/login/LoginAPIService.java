package andrej.biro.nipme.networking.authentication.login;

import andrej.biro.nipme.model.LoggedUser;
import andrej.biro.nipme.networking.authentication.login.exception.LoginTechFailureException;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by andrej on 17.11.2016.
 */

public class LoginAPIService {
    private ILoginApi loginApi;
    private boolean isRequestingLogin;


    public LoginAPIService(Retrofit retrofit) {
        this.loginApi = retrofit.create(ILoginApi.class);

    }

    public Maybe<LoggedUser> login(LoginRequest request) {

        return loginApi.login(request)
                .doOnSubscribe(disposable -> isRequestingLogin = true)
                .doOnTerminate(() -> isRequestingLogin = false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::handleLoginError)
                .singleElement();
    }

    public boolean isRequestingLogin() {
        return isRequestingLogin;
    }

    private Observable<LoggedUser> handleLoginError(Throwable throwable) {

        throw new LoginTechFailureException();
    }
}
