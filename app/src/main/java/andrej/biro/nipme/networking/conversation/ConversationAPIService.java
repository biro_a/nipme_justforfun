package andrej.biro.nipme.networking.conversation;

import java.util.List;

import andrej.biro.nipme.model.Message;
import andrej.biro.nipme.networking.authentication.login.exception.LoginTechFailureException;
import io.reactivex.Maybe;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

/**
 * Created by andrej on 7.1.2017.
 */

public class ConversationAPIService {
    private IConversationApi conversationApi;
    private boolean isRequesting;

    public ConversationAPIService(Retrofit retrofit) {
        conversationApi = retrofit.create(IConversationApi.class);
    }

    public Maybe<List<Message>> getMessages(String token, ConversationRequest request) {
        return conversationApi.getConversation(token, request)
                .doOnSubscribe(disposable -> isRequesting = true)
                .doOnTerminate(() -> isRequesting = false)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(this::handleLoginError)
                .singleElement();
    }

    public boolean isRequesting() {
        return isRequesting;
    }

    private Observable<List<Message>> handleLoginError(Throwable throwable) {

        throw new RuntimeException();
    }
}
