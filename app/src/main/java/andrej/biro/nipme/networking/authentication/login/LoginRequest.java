package andrej.biro.nipme.networking.authentication.login;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andrej on 12.11.2016.
 */

public class LoginRequest {
    @SerializedName("Login") private String login;
    @SerializedName("Password") private String password;

    public LoginRequest() {}

    public LoginRequest(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
