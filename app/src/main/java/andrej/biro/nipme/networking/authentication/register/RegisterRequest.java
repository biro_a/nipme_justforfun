package andrej.biro.nipme.networking.authentication.register;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andrej on 26.11.2016.
 */

public class RegisterRequest {
    @SerializedName("Login") private String login;
    @SerializedName("Password") private String password;
    @SerializedName("Motto") private String motto;

    public RegisterRequest() {}

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }
}
