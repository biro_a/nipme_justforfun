package andrej.biro.nipme.networking.authentication.register;

import andrej.biro.nipme.model.LoggedUser;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by andrej on 26.11.2016.
 */

interface IRegisterApi {
    @POST("ws/AuthService.svc/register")
    Observable<LoggedUser> register(@Body RegisterRequest request);
}
