package andrej.biro.nipme.networking.conversation;

import com.google.gson.annotations.SerializedName;

/**
 * Created by andrej on 7.1.2017.
 */

public class ConversationRequest {
    @SerializedName("UserId")
    private int userId;
    @SerializedName("FirstItemIndex")
    private int firstItemIndex;
    @SerializedName("ItemsCount")
    private int itemCount;

    public ConversationRequest(int userId, int firstItemIndex, int itemCount) {
        this.userId = userId;
        this.firstItemIndex = firstItemIndex;
        this.itemCount = itemCount;
    }

    public int getUserId() {
        return userId;
    }

    public int getFirstItemIndex() {
        return firstItemIndex;
    }

    public int getItemCount() {
        return itemCount;
    }
}
