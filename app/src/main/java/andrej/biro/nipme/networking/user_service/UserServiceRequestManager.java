package andrej.biro.nipme.networking.user_service;

import java.util.List;

import andrej.biro.nipme.model.User;
import andrej.biro.nipme.networking.RetrofitFactory;
import andrej.biro.nipme.networking.user_service.find.FindUsersAPIService;
import andrej.biro.nipme.networking.user_service.find.FindUsersRequest;
import io.reactivex.Single;
import retrofit2.Retrofit;

/**
 * Created by andrej on 25.12.2016.
 */

public class UserServiceRequestManager {
    private static UserServiceRequestManager instance;
    private FindUsersAPIService findUsersAPIService;

    public static UserServiceRequestManager getInstance() {
        synchronized (UserServiceRequestManager.class) {
            if (instance == null) {
                instance = new UserServiceRequestManager();
            }
        }

        return instance;
    }

    private UserServiceRequestManager() {
        Retrofit retrofit = RetrofitFactory.getAdapter();
        findUsersAPIService = new FindUsersAPIService(retrofit);
    }

    public Single<List<User>> findUsers(String token, FindUsersRequest request) {
        return findUsersAPIService.findUsers(token, request);
    }


}
