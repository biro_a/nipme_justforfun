package andrej.biro.nipme.networking.conversation;

import java.util.List;

import andrej.biro.nipme.model.Message;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by andrej on 7.1.2017.
 */

public interface IConversationApi {
    @POST("ws/MessageService.svc/getMessages")
    Observable<List<Message>> getConversation(@Query("token") String token, @Body ConversationRequest request);
}
