package andrej.biro.nipme.networking.authentication;

import andrej.biro.nipme.model.LoggedUser;
import andrej.biro.nipme.networking.RetrofitFactory;
import andrej.biro.nipme.networking.authentication.login.LoginAPIService;
import andrej.biro.nipme.networking.authentication.login.LoginRequest;
import andrej.biro.nipme.networking.authentication.register.RegisterAPIService;
import andrej.biro.nipme.networking.authentication.register.RegisterRequest;
import io.reactivex.MaybeSource;
import retrofit2.Retrofit;

/**
 * Created by andrej on 17.11.2016.
 */

public class AuthenticationRequestManager {
    private static AuthenticationRequestManager instance;
    private LoginAPIService loginAPIService;
    private RegisterAPIService registerAPIService;

    public static AuthenticationRequestManager getInstance() {
        synchronized (AuthenticationRequestManager.class) {
            if (instance == null) {
                instance = new AuthenticationRequestManager();
            }
        }
        return instance;
    }

    private AuthenticationRequestManager() {
        Retrofit retrofit = RetrofitFactory.getAdapter();
        loginAPIService = new LoginAPIService(retrofit);
        registerAPIService = new RegisterAPIService(retrofit);
    }

    public MaybeSource<LoggedUser> login(LoginRequest request) {
        return loginAPIService.login(request);
    }

    public MaybeSource<LoggedUser> register(RegisterRequest request) { return  registerAPIService.register(request); }

    public boolean isRequestingInformation() {
        return loginAPIService.isRequestingLogin() ||
                registerAPIService.isRequestingRegister();
    }
}
