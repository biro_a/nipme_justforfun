package andrej.biro.nipme.networking.conversation;

import java.util.List;

import andrej.biro.nipme.model.Message;
import andrej.biro.nipme.networking.RetrofitFactory;
import io.reactivex.Maybe;
import retrofit2.Retrofit;

/**
 * Created by andrej on 7.1.2017.
 */

public class ConversationRequestManager {
    private static ConversationRequestManager instance;
    private ConversationAPIService conversationService;

    public static ConversationRequestManager getInstance() {
        synchronized (ConversationRequestManager.class) {
            if (instance == null) {
                instance = new ConversationRequestManager();
            }
        }

        return instance;
    }

    private ConversationRequestManager() {
        Retrofit retrofit = RetrofitFactory.getAdapter();
        conversationService = new ConversationAPIService(retrofit);
    }

    public Maybe<List<Message>> getConversation(String token, ConversationRequest request) {
        return conversationService.getMessages(token, request);
    }
}
