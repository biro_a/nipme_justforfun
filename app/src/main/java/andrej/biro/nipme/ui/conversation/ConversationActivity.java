package andrej.biro.nipme.ui.conversation;

import android.support.v4.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import andrej.biro.nipme.R;
import andrej.biro.nipme.model.User;
import eu.inloop.viewmodel.IViewModelProvider;
import eu.inloop.viewmodel.ViewModelProvider;

public class ConversationActivity extends AppCompatActivity implements IViewModelProvider, ConversationHandler {
    private static final String CONVERSATION_FRAGMENT_TAG = "CONVERSATION_FRAGMENT_TAG";
    private static final String USER_BUNDLE_KEY = "USER";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        User user = null;
        if (getIntent() != null) {
            user = (User) getIntent().getSerializableExtra(USER_BUNDLE_KEY);
        }

        assert user != null;

        ConversationFragment fragment = ConversationFragment.newInstance(user);
        addFragmentToContainer(fragment, R.id.container, CONVERSATION_FRAGMENT_TAG);

        setSupportActionBar(toolbar);
    }

    @Override
    public ViewModelProvider getViewModelProvider() {
        return ViewModelProvider.newInstance(this);
    }

    public static Intent createIntent(Context context, User user) {
        Intent intent = new Intent(context, ConversationActivity.class);
        intent.putExtra(USER_BUNDLE_KEY, user);
        return intent;
    }

    protected void addFragmentToContainer(Fragment fragment, @IdRes int containerId, String tag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(containerId, fragment, tag);
        transaction.commit();
    }
}
