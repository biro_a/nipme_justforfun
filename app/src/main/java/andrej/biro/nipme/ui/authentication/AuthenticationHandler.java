package andrej.biro.nipme.ui.authentication;

import andrej.biro.nipme.base.BaseHandler;

/**
 * Created by andrej on 24.11.2016.
 */

public interface AuthenticationHandler extends BaseHandler{
    void startMainActivity();
}
