package andrej.biro.nipme.ui.authentication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.GestureDetector;
import android.view.MotionEvent;

import andrej.biro.nipme.R;
import andrej.biro.nipme.ui.main.MainActivity;
import eu.inloop.viewmodel.IViewModelProvider;
import eu.inloop.viewmodel.ViewModelProvider;

public class AuthenticationActivity extends AppCompatActivity implements AuthenticationHandler, IViewModelProvider{
    private AuthenticationFragment fragment;
    private GestureDetector gestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);
        fragment = (AuthenticationFragment) getSupportFragmentManager().findFragmentById(R.id.authentication_fragment);
        gestureDetector = new GestureDetector(this,  new OnSwipeGestureListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        if (fragment.isAuthenticationViewVisible()) {
            fragment.handleSwipeLeftToRight();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public ViewModelProvider getViewModelProvider() {
        return ViewModelProvider.newInstance(this);
    }

    @Override
    public void startMainActivity() {
        Intent i = MainActivity.createIntent(getBaseContext());
        startActivity(i);
        finish();
    }

    /********************
     *                   *
     *   inner classes   *
     *                   *
     *********************/
    private class OnSwipeGestureListener extends
            GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {

            float deltaX = e2.getX() - e1.getX();
            if ((Math.abs(deltaX) < 120) || (Math.abs(velocityX) < 200)) {
                return false; // insignificant swipe
            } else {
                if (deltaX > 0) { // right to left
                    fragment.handleSwipeLeftToRight();
                }
            }
            return true;
        }
    }
}
