package andrej.biro.nipme.ui.users;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import andrej.biro.nipme.R;
import andrej.biro.nipme.base.BaseFragment;
import andrej.biro.nipme.databinding.FragmentUserListBinding;
import andrej.biro.nipme.databinding.ItemUserBinding;
import andrej.biro.nipme.model.User;
import andrej.biro.nipme.utils.OnItemClickListener;
import andrej.biro.nipme.viewmodel.users.IUserListView;
import andrej.biro.nipme.viewmodel.users.UserListViewModel;

public class UserListFragment extends BaseFragment<IUserListView, UserListViewModel, FragmentUserListBinding, UserListHandler>
        implements  SwipeRefreshLayout.OnRefreshListener, IUserListView{

    private static final String POSITION_BUNDLE_KEY = "POSITION";

    private UserAdapter userAdapter;

    private int position;


    public static UserListFragment getInstance(int position) {
        Bundle args = new Bundle();
        args.putInt(POSITION_BUNDLE_KEY, position);
        UserListFragment fragment = new UserListFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            position = getArguments().getInt(POSITION_BUNDLE_KEY, 0);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_user_list;
    }

    @NonNull
    @Override
    protected IUserListView getViewClass() {
        return this;
    }

    @Override
    protected void setModelViewData() {
        getViewModel().initiate(position);
    }

    @Override
    protected void alterViews() {
        view.userList.setLayoutManager(new LinearLayoutManager(getContext()));
        view.swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getViewModel().onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        getViewModel().onPause();
    }

    @Nullable
    @Override
    public Class<UserListViewModel> getViewModelClass() {
        return UserListViewModel.class;
    }

    @Override
    public void onRefresh() {
        getViewModel().findUsers();
    }

    @Override
    public void creteAdapter(List<User> users) {
        userAdapter = new UserAdapter(getContext(), users, (view1, position1) -> {
            User user = getViewModel().getUser(position1);
            handler.startConversationActivity(user);
        });
        view.userList.setAdapter(userAdapter);
    }

    @Override
    public void updateAdapter() {
        view.swipeRefreshLayout.setRefreshing(false);
        userAdapter.notifyDataSetChanged();
    }


    /********************
    *                   *
    *   inner classes   *
    *                   *
    *********************/

    private static class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserHolder> {
        private List<User> userList;
        private Context context;
        private OnItemClickListener itemClickListener;

        UserAdapter(Context context, List<User> userList, OnItemClickListener itemClickListener) {
            this.context = context;
            this.userList = userList;
            this.itemClickListener = itemClickListener;
        }

        @Override
        public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.item_user, parent, false);
            return new UserHolder(view);
        }

        @Override
        public void onBindViewHolder(UserHolder viewHolder, int position) {
            User user = userList.get(position);
            viewHolder.view.setUser(user);

            Picasso.with(context)
                    .load(user.getProfilePhotoUrl())
                    .placeholder(R.drawable.layout_conversation_user)
                    .into(viewHolder.view.userPhoto);
        }



        @Override
        public int getItemCount() {
            return userList.size();
        }

        class UserHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
            private ItemUserBinding view;

            UserHolder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);
                view = ItemUserBinding.bind(itemView);
            }

            @Override
            public void onClick(View view) {
               if (itemClickListener != null) {
                   itemClickListener.onItemClick(view, getAdapterPosition());
               }
            }
        }
    }

}
