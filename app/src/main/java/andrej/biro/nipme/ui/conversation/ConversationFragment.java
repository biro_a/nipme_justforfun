package andrej.biro.nipme.ui.conversation;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import andrej.biro.nipme.R;
import andrej.biro.nipme.base.BaseFragment;
import andrej.biro.nipme.databinding.FragmentConversationBinding;
import andrej.biro.nipme.databinding.ItemMessageInBinding;
import andrej.biro.nipme.databinding.ItemMessageOutBinding;
import andrej.biro.nipme.model.Message;
import andrej.biro.nipme.model.User;
import andrej.biro.nipme.viewmodel.conversation.ConversationViewModel;
import andrej.biro.nipme.viewmodel.conversation.IConversationView;

public class ConversationFragment extends BaseFragment<IConversationView, ConversationViewModel, FragmentConversationBinding, ConversationHandler>
        implements IConversationView, SwipeRefreshLayout.OnRefreshListener {
    private static final String USER_ID_BUNDLE_KEY = "USER_ID";

    private User user;
    private MessageAdapter messageAdapter;

    public ConversationFragment() {}

    /********************
    *                   *
    *   implementation  *
    *                   *
    *********************/
    @Nullable
    @Override
    public Class<ConversationViewModel> getViewModelClass() {
        return ConversationViewModel.class;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            user = (User) getArguments().getSerializable(USER_ID_BUNDLE_KEY);
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_conversation;
    }

    @NonNull
    @Override
    protected IConversationView getViewClass() {
        return this;
    }

    @Override
    protected void setModelViewData() {
        getViewModel().initiate(user);
    }

    @Override
    protected void alterViews() {
        view.messageList.setLayoutManager(new LinearLayoutManager(getContext()));
        view.swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        getViewModel().loadMessages();
    }

    @Override
    public void createAdapter(List<Message> messages) {
        messageAdapter = new MessageAdapter(messages);
        view.messageList.setAdapter(messageAdapter);
    }

    @Override
    public void updateAdapter() {
        messageAdapter.notifyDataSetChanged();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public static ConversationFragment newInstance(@NonNull User user) {
        ConversationFragment fragment = new ConversationFragment();
        Bundle args = new Bundle();
        args.putSerializable(USER_ID_BUNDLE_KEY, user);
        fragment.setArguments(args);
        return fragment;
    }


    /********************
    *                   *
    *   inner classes   *
    *                   *
    *********************/

    private static class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {
        private static final int IN_MESSAGE = 0;
        private static final int OUT_MESSAGE = 1;

        private List<Message> messages;

        MessageAdapter(List<Message> messages) {
            this.messages = messages;
        }

        @Override
        public int getItemViewType(int position) {
            return messages.get(position).isMessageMy() ? IN_MESSAGE : OUT_MESSAGE;
        }

        @Override
        public MessageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            MessageHolder holder;

            switch (viewType) {
                case IN_MESSAGE: {
                    View view = inflater.inflate(R.layout.item_message_in, parent, false);
                    holder = new InMessageHolder(view);
                    break;
                }
                case OUT_MESSAGE: {
                    View view = inflater.inflate(R.layout.item_message_out, parent, false);
                    holder = new OutMessageHolder(view);
                    break;
                }
                default: throw new IllegalStateException("Unknown view type: " + viewType);
            }

            return holder;
        }

        @Override
        public void onBindViewHolder(MessageHolder holder, int position) {
            int viewType = getItemViewType(position);

            switch (viewType) {
                case IN_MESSAGE:
                    ((InMessageHolder)holder).view.setMessage(messages.get(position));
                    break;
                case OUT_MESSAGE:
                    ((OutMessageHolder)holder).view.setMessage(messages.get(position));
                    break;
                default: throw new IllegalStateException("Unknown view type: " + viewType);
            }
        }

        @Override
        public int getItemCount() {
            return messages.size();
        }

        static class MessageHolder extends RecyclerView.ViewHolder {

            MessageHolder(View itemView) {
                super(itemView);
            }
        }

        static class InMessageHolder extends MessageHolder {
            private ItemMessageInBinding view;

            InMessageHolder(View itemView) {
                super(itemView);
                view = ItemMessageInBinding.bind(itemView);
            }
        }

        static class OutMessageHolder extends MessageHolder {
            private ItemMessageOutBinding view;

            OutMessageHolder(View itemView) {
                super(itemView);
                view = ItemMessageOutBinding.bind(itemView);
            }
        }
    }


}
