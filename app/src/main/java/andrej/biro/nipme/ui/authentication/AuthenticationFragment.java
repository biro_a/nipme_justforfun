package andrej.biro.nipme.ui.authentication;

import android.content.Context;
import android.graphics.Point;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;

import andrej.biro.nipme.R;
import andrej.biro.nipme.base.BaseFragment;
import andrej.biro.nipme.databinding.FragmentAuthenticationBinding;
import andrej.biro.nipme.networking.NetworkError;
import andrej.biro.nipme.viewmodel.authentication.AuthenticationViewModel;
import andrej.biro.nipme.viewmodel.authentication.IAuthenticationView;


/**
 * Authentication Fragment
 */
public class AuthenticationFragment extends BaseFragment<IAuthenticationView, AuthenticationViewModel, FragmentAuthenticationBinding, AuthenticationHandler> implements IAuthenticationView {

    /********************
    *                   *
    *      fields       *
    *                   *
    *********************/
    private Animation animSpinnerIn;
    private Animation animSpinnerOut;
    private Animation animSlideDownAndFadeIn;

    private boolean isLoginViewVisible;
    private boolean isRegisterViewVisible;

    /********************
    *                   *
    *   implementation  *
    *                   *
    *********************/
    @Nullable
    @Override
    public Class<AuthenticationViewModel> getViewModelClass() {
        return AuthenticationViewModel.class;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_authentication;
    }

    @NonNull
    @Override
    protected IAuthenticationView getViewClass() {
        return this;
    }

    @Override
    protected void setModelViewData() {
        getViewModel().initialize();
        view.setViewModel(getViewModel());

    }

    @Override
    protected void alterViews() {
        showFooterView();

        animSpinnerIn = AnimationUtils.loadAnimation(getContext(), R.anim.spinner_in);
        animSpinnerOut = AnimationUtils.loadAnimation(getContext(), R.anim.spinner_out);
        animSlideDownAndFadeIn = AnimationUtils.loadAnimation(getContext(), R.anim.slide_down_and_fade_in);
    }

    /********************
    *                   *
    *    view's method  *
    *                   *
    *********************/
    @Override
    public void showLoginView() {
        isLoginViewVisible = true;
        animateAuthenticateAndFooterViews(view.loginView.loginView, -view.footerView.getWidth(), 0);
    }

    @Override
    public void showRegisterView() {
        isRegisterViewVisible = true;
        animateAuthenticateAndFooterViews(view.registerView.registerView, -view.footerView.getWidth(), 0);
    }

    @Override
    public void startSpinnerAnimation() {
        // hide keyboard
        View view = AuthenticationFragment.this.getView();
        if (view != null) {

            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        this.view.spinnerIn.startAnimation(animSpinnerIn);
        this.view.spinnerOut.startAnimation(animSpinnerOut);
    }

    @Override
    public void successAuthenticate() {
        new Handler().postDelayed(() -> {
            stopSpinnerAnimation();
            if (handler != null) {
                handler.startMainActivity();
            }
        }, 3000);
    }

    @Override
    public void authenticationValidationInputFailed(@NonNull LoginValidationInputErrorType errorType) {
        stopSpinnerAnimation();
        switch (errorType) {
            case LOGIN_USERNAME_TOO_SHORT:
                view.loginView.loginUsername.setError(getString(errorType.errorString));
                break;
            case LOGIN_PASSWORD_TOO_SHORT:
                view.loginView.loginPassword.setError(getString(errorType.errorString));
                break;
            case REGISTER_USERNAME_TOO_SHORT:
                view.registerView.registerUsername.setError(getString(errorType.errorString));
                break;
            case REGISTER_PASSWORD_TOO_SHORT:
                view.registerView.registerPassword.setError(getString(errorType.errorString));
                break;
        }
    }

    @Override
    public void onAuthenticationError(@NonNull NetworkError error) {
        // slide down animation for error view and showing error text
        view.errorView.setVisibility(View.VISIBLE);
        animSlideDownAndFadeIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                view.errorView.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        stopSpinnerAnimation();
        view.errorView.startAnimation(animSlideDownAndFadeIn);
        view.setAuthenticationError(error);
    }

    /********************
    *                   *
    *   public methods  *
    *                   *
    *********************/
    public boolean isAuthenticationViewVisible() { return isLoginViewVisible || isRegisterViewVisible; }

    public void handleSwipeLeftToRight() {

        if (isLoginViewVisible) {
            isLoginViewVisible = false;
            View view = this.view.loginView.loginView;
            animateAuthenticateAndFooterViews(view, 0, view.getWidth());
        } else if (isRegisterViewVisible) {
            isRegisterViewVisible = false;
            View view = this.view.registerView.registerView;
            animateAuthenticateAndFooterViews(view, 0, view.getWidth());
        }
    }

    /********************
    *                   *
    *  private methods  *
    *                   *
    *********************/
    private void showFooterView() {

        if (getActivity() != null) {
            Display display = getActivity().getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);

            view.loginView.loginView.setX(size.x);
            view.registerView.registerView.setX(size.x);
            view.footerView.setY(size.y);
            view.footerView.animate().setDuration(700).translationY(0).withLayer();
        }
    }

    private void animateAuthenticateAndFooterViews(View authenticateView, int footerXPos, int authenticateXPos) {
        view.footerView.animate().setDuration(300);
        view.footerView.animate().translationX(footerXPos);
        authenticateView.animate().translationX(authenticateXPos).withLayer();
    }

    private void stopSpinnerAnimation() {
        view.spinnerIn.clearAnimation();
        view.spinnerOut.clearAnimation();
    }

    public enum LoginValidationInputErrorType {
        LOGIN_USERNAME_TOO_SHORT(R.string.username_too_short),
        LOGIN_PASSWORD_TOO_SHORT(R.string.password_too_short),
        REGISTER_USERNAME_TOO_SHORT(R.string.username_too_short),
        REGISTER_PASSWORD_TOO_SHORT(R.string.password_too_short);

        private @StringRes int errorString;

        LoginValidationInputErrorType(int errorString) {
            this.errorString = errorString;
        }
    }
}
