package andrej.biro.nipme.ui.users;

import andrej.biro.nipme.base.BaseHandler;
import andrej.biro.nipme.model.User;

/**
 * Created by andrej on 17.12.2016.
 */

public interface UserListHandler extends BaseHandler{
     void startConversationActivity(User user);
}
